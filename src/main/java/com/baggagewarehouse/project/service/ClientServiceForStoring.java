package com.baggagewarehouse.project.service;

import com.baggagewarehouse.project.model.Baggage;
import com.baggagewarehouse.project.model.Client;
import com.baggagewarehouse.project.model.Warehouse;
import com.baggagewarehouse.project.repository.ClientRepository;
import com.baggagewarehouse.project.repository.WarehouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

@Service
@RequiredArgsConstructor
public class ClientServiceForStoring {

    private final ClientRepository clientRepository;
    private final WarehouseRepository warehouseRepository;
    private final Scanner scanner = new Scanner(System.in);

    public Client saveClient() {
        String nickname = getClientNickname();
        String numberOfLuggage = getNumberOfLuggage();
        String cod = generateRetrieverCod();
        System.out.println("Your retriever cod is: " + cod);
        Client client = new Client();
        Warehouse warehouse = warehouseRepository.getOne(1L);
        List<Baggage> luggage = baggageList(numberOfLuggage, client, warehouse);
        client.setNickname(nickname);
        client.setCod(cod);
        client.setBags(luggage);
        clientRepository.save(client);
        return client;
    }

    private String getClientNickname() {
        System.out.println("Enter your nickname");
        String clientNickname = scanner.nextLine();
        while ("".equals(clientNickname)) {
            System.out.println("Enter a nickname");
            clientNickname = scanner.nextLine();
        }
        return clientNickname;
    }

    private String getNumberOfLuggage() {
        System.out.println("Enter the number of luggage to store. The maximum number is 3");
        String numberOfLuggage = scanner.nextLine();
        while (!"1".equals(numberOfLuggage) && !"2".equals(numberOfLuggage) && !"3".equals(numberOfLuggage)) {
            System.out.println("Invalid number! You need to store at least 1 luggage or 3 max");
            numberOfLuggage = scanner.nextLine();
        }
        return numberOfLuggage;
    }

    private String generateRetrieverCod() {
        String baseCod = "ISA";
        Random random = new Random();
        return baseCod + random.nextInt(9999);
    }

    private List<Baggage> baggageList(String numberOfLuggage, Client client, Warehouse warehouse) {
        List<Baggage> luggage = new ArrayList<>();
        switch (numberOfLuggage) {
            case "1":
                Baggage baggage1 = createBaggage(client, warehouse);
                luggage.add(baggage1);
                break;
            case "2":
                for (int i = 0; i < 2; i++) {
                    Baggage baggage = createBaggage(client, warehouse);
                    luggage.add(baggage);
                }
                break;
            case "3":
                for (int i = 0; i < 3; i++) {
                    Baggage baggage = createBaggage(client, warehouse);
                    luggage.add(baggage);
                }
                break;
        }
        return luggage;
    }

    private Baggage createBaggage(Client client, Warehouse warehouse) {
        Baggage baggage = new Baggage();
        baggage.setClient(client);
        baggage.setWarehouse(warehouse);
        baggage.setEntryDate(getDateAndTime());
        return baggage;
    }

    private LocalDateTime getDateAndTime() {
        return LocalDateTime.now();
    }
}
