package com.baggagewarehouse.project.service;

import com.baggagewarehouse.project.model.Client;
import com.baggagewarehouse.project.validation.UserValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

@Service
@RequiredArgsConstructor
public class ClientServiceForRetrieving {

    private final UserValidation userValidation;
    private final Scanner scanner = new Scanner(System.in);

    public Client retrieveLuggage() {
        Client client = userValidation.getCurrentClient();
        displayClient(client);
        Duration duration = luggageStorageTime(client);
        double paymentAmount = paymentAmount(client, duration);
        paymentImplementation(paymentAmount);
        return client;
    }

    private void displayClient(Client client) {
        LocalDateTime baggageEntryDate = client.getBags().get(0).getEntryDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String entryDateTime = baggageEntryDate.format(formatter);
        System.out.println("Client: " + client.getNickname() + ", number of bags: " + client.getBags().size() +
                ", entry date: " + entryDateTime);
    }

    private Duration luggageStorageTime(Client client) {
        Duration duration = Duration.between(client.getBags().get(0).getEntryDate(), LocalDateTime.now());
        if (duration.toDaysPart() > 0) {
            System.out.println("Your luggage was stored " + duration.toDaysPart() + " days, "
                    + duration.toHoursPart() + " hours " + duration.toMinutesPart() + " minutes");
        } else if (duration.toHoursPart() > 0) {
            System.out.println("Your luggage was stored " + duration.toHoursPart() + " hours "
                    + duration.toMinutesPart() + " minutes");
        } else {
            System.out.println("Your luggage was stored " + duration.toMinutesPart() + " minutes");
        }
        return duration;
    }

    private double paymentAmount(Client client, Duration duration) {
        double amountToPay = 0;
        long luggageStorageTimeAsHours = duration.toHoursPart();
        if (luggageStorageTimeAsHours == 0) {
            amountToPay = 10 * client.getBags().size();
        } else {
            amountToPay = (10 + (5 * luggageStorageTimeAsHours)) * client.getBags().size();
        }
        return amountToPay;
    }

    private void paymentImplementation(double paymentAmount) {
        System.out.println("You owe " + paymentAmount + " RON.");
        System.out.println("Please deposit the amount that you owe!");
        double clientInput = scanner.nextDouble();
        double totalClientInput = clientInput;
        while (totalClientInput < paymentAmount) {
            double paymentAmountRemaining = paymentAmount - totalClientInput;
            System.out.println("You still have to pay " + paymentAmountRemaining + " RON.");
            clientInput = scanner.nextDouble();
            totalClientInput += clientInput;
        }
        if (totalClientInput == paymentAmount) {
            System.out.println("Here is your luggage!");
        } else {
            double amountDue = totalClientInput - paymentAmount;
            System.out.println("Here is your change " + amountDue + " RON, and your luggage!");
        }
    }
}
