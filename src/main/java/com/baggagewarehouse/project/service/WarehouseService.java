package com.baggagewarehouse.project.service;

import com.baggagewarehouse.project.model.Client;
import com.baggagewarehouse.project.repository.WarehouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WarehouseService {

    private final WarehouseRepository warehouseRepository;

    public void updateWarehouseTotalStorageUnits(Client client, String string) {
        int totalStorageUnits = warehouseRepository.getTotalStorageUnits();
        int baggageNumber = client.getBags().size();
        if (string.equals("store")) {
            int newStorageUnits = totalStorageUnits - baggageNumber;
            warehouseRepository.updateWarehouseStorageUnits(newStorageUnits);
        } else if (string.equals("retrieve")) {
            int newStorageUnits = totalStorageUnits + baggageNumber;
            warehouseRepository.updateWarehouseStorageUnits(newStorageUnits);
        }
    }
}
