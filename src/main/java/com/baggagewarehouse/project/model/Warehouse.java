package com.baggagewarehouse.project.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "warehouse")
@Data
public class Warehouse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ToString.Exclude
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "total_storage_units")
    private Integer totalStorageUnits;

    @OneToMany(mappedBy = "warehouse", cascade = CascadeType.ALL)
    private List<Baggage> bags;
}
