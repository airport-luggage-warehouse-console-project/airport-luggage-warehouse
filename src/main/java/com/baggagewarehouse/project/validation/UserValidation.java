package com.baggagewarehouse.project.validation;

import com.baggagewarehouse.project.model.Client;
import com.baggagewarehouse.project.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Optional;
import java.util.Scanner;

@Component
@RequiredArgsConstructor
public class UserValidation {

    private final Scanner scanner = new Scanner(System.in);
    private final ClientRepository clientRepository;

    public Client getCurrentClient() {
        String cod = getRetrieverCodFromClient();
        Optional<Client> client = clientRepository.findClientByCod(cod);
        while (client.isEmpty() || !cod.equals(client.get().getCod())) {
            System.out.println("Cod incorrect");
            cod = scanner.nextLine().toUpperCase(Locale.ROOT);
            client = clientRepository.findClientByCod(cod);
        }
        return client.get();
    }

    private String getRetrieverCodFromClient() {
        System.out.println("Enter the cod to retrieve your luggage");
        return scanner.nextLine().toUpperCase(Locale.ROOT);
    }
}
