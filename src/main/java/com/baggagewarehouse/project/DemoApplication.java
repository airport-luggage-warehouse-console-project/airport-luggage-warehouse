package com.baggagewarehouse.project;

import com.baggagewarehouse.project.utils.UserInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    private final UserInterface userInterface;

    @Autowired
    public DemoApplication(UserInterface userInterface) {
        this.userInterface = userInterface;
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) {
        userInterface.menu();
    }
}
