package com.baggagewarehouse.project.utils;

import com.baggagewarehouse.project.model.Client;
import com.baggagewarehouse.project.repository.ClientRepository;
import com.baggagewarehouse.project.repository.WarehouseRepository;
import com.baggagewarehouse.project.service.ClientServiceForRetrieving;
import com.baggagewarehouse.project.service.ClientServiceForStoring;
import com.baggagewarehouse.project.service.WarehouseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
@RequiredArgsConstructor
public class UserInterface {

    private final WarehouseRepository warehouseRepository;
    private final ClientServiceForStoring clientServiceForStoring;
    private final ClientServiceForRetrieving clientServiceForRetrieving;
    private final WarehouseService warehouseService;
    private final ClientRepository clientRepository;
    private final Scanner scanner = new Scanner(System.in);

    public void menu() {
        while (true) {
            menuBeginning();
            String userInput = getUserInput();
            if ("1".equals(userInput)) {
                Client client = clientServiceForStoring.saveClient();
                String store = "store";
                warehouseService.updateWarehouseTotalStorageUnits(client, store);
            } else if ("2".equals(userInput)) {
                Client client = clientServiceForRetrieving.retrieveLuggage();
                String retrieve = "retrieve";
                warehouseService.updateWarehouseTotalStorageUnits(client, retrieve);
                clientRepository.deleteById(client.getId());
            }
            endMessage();
            System.out.println();
        }
    }

    private void menuBeginning() {
        System.out.println("Welcome to Iasi Airport Warehouse");
        System.out.println("Our prices are: 10 RON first hour, then 5 RON an hour.");
        System.out.println("Available storage units: " + warehouseRepository.getTotalStorageUnits());
    }

    private String getUserInput() {
        System.out.println("Press 1 to store your luggage or press 2 to retrieve your luggage");
        String userInput = scanner.nextLine();
        while (!"1".equals(userInput) && !"2".equals(userInput)) {
            System.out.println("Invalid number! Enter 1 to store or 2 to retrieve your luggage");
            userInput = scanner.nextLine();
        }
        return userInput;
    }

    private void endMessage() {
        System.out.println("Thank you for using our services!");
    }
}

