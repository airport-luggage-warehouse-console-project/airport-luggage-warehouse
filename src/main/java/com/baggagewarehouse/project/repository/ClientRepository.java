package com.baggagewarehouse.project.repository;

import com.baggagewarehouse.project.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<Client> findClientByCod(String cod);

    @Transactional
    void deleteById(Long id);
}
