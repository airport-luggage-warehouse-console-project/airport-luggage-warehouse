package com.baggagewarehouse.project.repository;

import com.baggagewarehouse.project.model.Warehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface WarehouseRepository extends JpaRepository<Warehouse, Long> {

    @Query("SELECT totalStorageUnits FROM Warehouse")
    int getTotalStorageUnits();

    @Transactional
    @Modifying
    @Query("UPDATE Warehouse SET totalStorageUnits = :newStorageUnits")
    void updateWarehouseStorageUnits(int newStorageUnits);
}
